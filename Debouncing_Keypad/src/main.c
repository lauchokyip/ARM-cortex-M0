/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/


#include "stm32f0xx.h"
#include "stm32f0_discovery.h"
			

int toggle = 0;

int col = 0;
int history[16] = {0};
uint32_t tick = 0;

void displayKey(int key) {
    GPIOC->ODR = key & 0xf;
}




void testDebounce() {
    init_GPIO();
    init_TIM6();
    while(1) {
        int key = getKeyPressed();
        if (key == getKeyReleased())
            displayKey(key);
    }
}



int main(void)
{
        testDebounce();
        while(1);
}


