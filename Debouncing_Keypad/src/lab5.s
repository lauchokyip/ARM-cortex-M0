.syntax unified
.cpu cortex-m0
.fpu softvfp
.thumb

.equ  RCC,      0x40021000
.equ  APB1ENR,  0x1C
.equ  AHBENR,   0x14
.equ  TIM6EN,	0x10
.equ  GPIOCEN,  0x00080000
.equ  GPIOBEN,  0x00040000
.equ  GPIOAEN,  0x00020000
.equ  GPIOC,    0x48000800
.equ  GPIOB,    0x48000400
.equ  GPIOA,    0x48000000
.equ  MODER,    0x00
.equ  PUPDR,    0x0c
.equ  IDR,      0x10
.equ  ODR,      0x14
.equ  PC0,      0x01
.equ  PC1,      0x04
.equ  PC2,      0x10
.equ  PC3,      0x40
.equ  PIN8,     0x00000100

// NVIC control registers...
.equ NVIC,		0xe000e000
.equ ISER, 		0x100
.equ ICER, 		0x180
.equ ISPR, 		0x200
.equ ICPR, 		0x280

// TIM6 control registers
.equ  TIM6, 	0x40001000
.equ  CR1,		0x00
.equ  DIER,		0x0C
.equ  PSC,		0x28
.equ  ARR,		0x2C
.equ  TIM6_DAC_IRQn, 17
.equ  SR,		0x10

.global init_TIM6
init_TIM6:
	push {lr}
	// enable system clock to TIM6
	ldr r0, =RCC
	ldr r1, [r0, #APB1ENR]
	movs r2, #TIM6EN
	orrs r1, r2
	str r1, [r0, #APB1ENR]
	// set PSC and ARR
	ldr r0, =TIM6
	ldr r1, =48-1
	str r1, [r0, #PSC]
	ldr r1, =1000-1
	str r1, [r0, #ARR]
	ldr r1, [r0, #CR1]
	movs r2, #1
	orrs r1, r2
	str r1, [r0, #CR1]
	// enable UIE in TIM6_DIER
	movs r1, #1
	str r1, [r0, #DIER]
	// enable TIM6 intr in NVIC_ISER
	ldr r0, =NVIC
	ldr r1, =ISER
	ldr r2, [r0, r1]
	movs r3, #1
	lsls r3, #TIM6_DAC_IRQn
	orrs r2, r3
	str r2, [r0, r1]
	pop {pc}

.global init_GPIO
init_GPIO:
	push {lr}
	ldr r0, =RCC
	ldr r1, [r0, #AHBENR]
	ldr r2, =GPIOAEN
	ldr r3, =GPIOCEN
	orrs r2, r3
	orrs r1, r2
	str r1, [r0, #AHBENR]
	ldr r0, =GPIOC
	ldr r1, [r0, #MODER]
	ldr r2, =0x10055
	orrs r1, r2
	str r1, [r0, #MODER]
	ldr r0, =GPIOA
	ldr r1, [r0, #MODER]
	ldr r2, =0x55
	orrs r1, r2
	str r1, [r0, #MODER]
	ldr r1, [r0, #PUPDR]
	ldr r2, =0xAA00
	orrs r1, r2
	str r1, [r0, #PUPDR]
	pop {pc}

.global getKeyReleased
getKeyReleased:
	push {lr}
while_release:
	movs r1,#0 // r0 as i
for_release:
	cmp r1,#16
	bge while_release
do_release:
	lsls r1,#2
	ldr r0,=history
	ldr r2,[r0,r1]
	lsrs r1,#2
	movs r3,#1
	mvns r3,r3
	cmp r2,r3
	beq if_release
	b not_release
if_release:
	ldr r0,=tick
	movs r2,#0
	str r2,[r0]
	b end_release_all
not_release:

end_release:
	adds r1,#1
	b for_release
end_release_all:
	movs r0,r1
	pop {pc}

.global getKeyPressed
getKeyPressed:
	push {lr}
while_press:
	movs r1,#0 // r0 as i
for_press:
	cmp r1,#16
	bge while_press
do_press:
	lsls r1,#2
	ldr r0,=history
	ldr r2,[r0,r1]
	lsrs r1,#2
	cmp r2,#1
	beq if_press
	b not_press
if_press:
	ldr r0,=tick
	movs r2,#0
	str r2,[r0]
	b end_press_all
not_press:

end_press:
	adds r1,#1
	b for_press
end_press_all:
	movs r0,r1
	pop {pc}


.type TIM6_DAC_IRQHandler, %function
.global TIM6_DAC_IRQHandler
TIM6_DAC_IRQHandler:
    push {r4,lr}
	// clear the flag
	ldr r0,=TIM6
	ldr r1,[r0,#SR]
	movs r2,#1
	bics r1,r2
	str r1,[r0,#SR]
	///////////////
	ldr r0,=tick
	ldr r1,[r0]
	adds r1,#1
	str r1,[r0]
check_tim6:
	ldr r0,=#1000
	cmp r1,r0
	beq do_tim6
	b end_tim6
do_tim6:
	ldr r0,=GPIOC
	ldr r1,[r0,#ODR]
	ldr r2,=0x100
	eors r1,r2
	str r1,[r0,#ODR]
	ldr r0,=tick
	movs r1,#0
	str r1,[r0]
end_tim6:
	ldr r0,=col
	ldr r1,[r0]
	adds r1,#1
	movs r2,#3
	ands r1,r2
	str r1,[r0]
	///////////
	ldr r0,=GPIOA
	movs r1,#1
	ldr r2,=col
	ldr r2,[r2]
	lsls r1,r2
	str r1,[r0,#ODR]
	//////////
	ldr r0,=col
	ldr r1,[r0]
	lsls r1,#2
	ldr r0,=index
	str r1,[r0]
	//////////
	ldr r0,=index
	ldr r1,[r0] //r1 has index

	//////////
	ldr r0,=history
    lsls r1,#2
    ldr r2,[r0,r1]
    lsls r2,#1
    str r2,[r0,r1]
    lsrs r1,#2
    //////////

	//////////
	ldr r0,=history
	adds r1,#1
    lsls r1,#2
    ldr r2,[r0,r1]
    lsls r2,#1
    str r2,[r0,r1]
    lsrs r1,#2
    //////////

	//////////
	ldr r0,=history
	adds r1,#1
    lsls r1,#2
    ldr r2,[r0,r1]
    lsls r2,#1
    str r2,[r0,r1]
    lsrs r1,#2
    //////////

	//////////
	adds r1,#1
	ldr r0,=history
    lsls r1,#2
    ldr r2,[r0,r1]
    lsls r2,#1
    str r2,[r0,r1]
    lsrs r1,#2
    //////////

	ldr r0,=GPIOA
	ldr r1,[r0,#IDR] // r1 is (GPIOA->IDR)
	lsrs r1,#4
	movs r2,#0xf
	ands r1,r2
	ldr r0,=row
	str r1,[r0]

	//////////
	ldr r0,=index
	ldr r1,[r0] //r1 has index

	//////////
	ldr r0,=history
    lsls r1,#2
    ldr r2,[r0,r1] // r2 has history[index]
    ldr r0,=row
    ldr r3,[r0]   // r3 has row
    ldr r4,=#0x1
    ands r3,r4    // ands value store to r3
    orrs r2,r3    // or the history
    ldr r0,=history
    str r2,[r0,r1]
    lsrs r1,#2
    //////////

	//////////
	ldr r0,=history
	adds r1,#1
    lsls r1,#2
    ldr r2,[r0,r1] // r2 has history[index]
    ldr r0,=row
    ldr r3,[r0]   // r3 has row
    lsrs r3,#1    // right shift 1
    ldr r4,=#0x1
    ands r3,r4    // ands value store to r3
    orrs r2,r3    // or the history
    ldr r0,=history
    str r2,[r0,r1]
    lsrs r1,#2
    //////////

	//////////
	ldr r0,=history
	adds r1,#1
    lsls r1,#2
    ldr r2,[r0,r1] // r2 has history[index]
    ldr r0,=row
    ldr r3,[r0]   // r3 has row
    lsrs r3,#2	   // right shift 2
    ldr r4,=#0x1
    ands r3,r4    // ands value store to r3
    orrs r2,r3    // or the history
    ldr r0,=history
    str r2,[r0,r1]
    lsrs r1,#2
    //////////

    //////////
	ldr r0,=history
	adds r1,#1
    lsls r1,#2
    ldr r2,[r0,r1] // r2 has history[index]
    ldr r0,=row
    ldr r3,[r0]   // r3 has row
    lsrs r3,#3     // right shift 3
    ldr r4,=#0x1
    ands r3,r4    // ands value store to r3
    orrs r2,r3    // or the history
    ldr r0,=history
    str r2,[r0,r1]
    lsrs r1,#2
    //////////
	pop {r4,pc}


.data
.align 4
row: .word 0
index: .word 0
