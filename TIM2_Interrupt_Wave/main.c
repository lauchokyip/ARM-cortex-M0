#include "stm32f0xx.h"
#include "stm32f0_discovery.h"
#include <stdio.h>
#include <stdlib.h>



#include <math.h>


int16_t wavetable[256];
int offset = 0;

void init_wavetable(void)
{
  int x;
  for(x=0; x< sizeof wavetable / sizeof wavetable[0] ; x++)
      wavetable[x] = 32767 * sin(2 * M_PI * x / 256);
}




// This function
// 1) enables clock to port A,
// 2) sets PA0, PA1, PA2 and PA4 to analog mode
void setup_gpio() {
    /* Student code goes here */
    RCC->AHBENR |= RCC_AHBENR_GPIOAEN;
    GPIOA->MODER |= (3 << 4*2);
}

// This function should enable the clock to the
// onboard DAC, enable trigger,
// setup software trigger and finally enable the DAC.
void setup_dac() {
    /* Student code goes here */
    RCC->APB1ENR |= RCC_APB1ENR_DACEN;
    DAC->CR &= ~DAC_CR_EN1;
    DAC->CR |= DAC_CR_TEN1;
    DAC->CR |= DAC_Trigger_Software;
    DAC->CR |= DAC_CR_EN1;
}

// This function should,
// enable clock to timer6,
// setup pre scalar and arr so that the interrupt is triggered every
// 10us, enable the timer 6 interrupt, and start the timer.
void setup_timer2() {
    /* Student code goes here */
    RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;
    // disable timer first
    TIM2 -> CR1 &= ~TIM_CR1_CEN;
    // set up pre scalar
    TIM2 -> PSC = 2-1;
    TIM2 -> ARR = 127-1;
    TIM2 -> DIER |= TIM_DIER_UIE;
    NVIC->ISER[0] = 1<< TIM2_IRQn;
    // enable timer
    TIM2 -> CR1 |= TIM_CR1_CEN;
}







void TIM2_IRQHandler() {
    /* Student code goes here */
    TIM2->SR &= ~1;

    if (offset >= (sizeof wavetable / sizeof wavetable[0]))
        offset = 0;

    int sample = (32768 + wavetable[offset]) >> 4;
    while((DAC->SWTRIGR & DAC_SWTRIGR_SWTRIG1) == DAC_SWTRIGR_SWTRIG1);
    DAC -> DHR12R1 = sample;
    DAC -> SWTRIGR |= DAC_SWTRIGR_SWTRIG1;
    offset++;

}

int main(void)
{
    /* Student code goes here */
    setup_gpio();
    setup_dac();
    init_wavetable();
    setup_timer2();
    while(1){

    }

}

