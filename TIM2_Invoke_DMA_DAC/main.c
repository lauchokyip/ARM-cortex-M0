#include "stm32f0xx.h"
#include "stm32f0_discovery.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>


int16_t wavetable[256];

void init_wavetable(void)
{
  int x;
  for(x=0; x< sizeof wavetable / sizeof wavetable[0] ; x++)
      wavetable[x] = (32767 * sin(2 * M_PI * x / 256) + 32768) / 16 ;
}





void setup_gpio() {
    /* Student code goes here */
    RCC->AHBENR |= RCC_AHBENR_GPIOAEN;
    GPIOA->MODER |= (3 << 4*2);
}


void setup_dac() {
    /* Student code goes here */
    RCC->APB1ENR |= RCC_APB1ENR_DACEN;
    DAC->CR &= ~DAC_CR_EN1;
    DAC->CR |= DAC_CR_TEN1;
    DAC->CR |= (DAC_CR_TSEL1_2);
    DAC->CR |= DAC_CR_DMAEN1;
    DAC->CR |= DAC_CR_EN1;
}


void setup_timer2() {
    /* Student code goes here */
    RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;
    // disable timer first
    TIM2 -> CR1 &= ~TIM_CR1_CEN;
    // set up pre scalar
    TIM2 -> PSC = 2-1;
    TIM2 -> ARR = 127-1;
    TIM2 -> CR2 |= TIM_CR2_MMS_1;
    TIM2 ->CR1 |= TIM_CR1_ARPE;
    TIM2 -> DIER  |= TIM_DIER_UDE;
    // enable timer
    TIM2 -> CR1 |= TIM_CR1_CEN;
}

void setup_dma(){
    RCC->AHBENR |= RCC_AHBENR_DMA1EN;
    DMA1_Channel2->CPAR = (uint32_t)(&(DAC->DHR12R1)) ;
    DMA1_Channel2->CMAR = (uint32_t)wavetable;
    DMA1_Channel2->CNDTR = sizeof wavetable / sizeof wavetable[0] ;
    DMA1_Channel2->CCR |= (DMA_CCR_MINC | DMA_CCR_MSIZE_0 | DMA_CCR_PSIZE_0 | DMA_CCR_CIRC | DMA_CCR_DIR);
    DMA1_Channel2->CCR |= DMA_CCR_EN;
}

int main(void)
{
    /* Student code goes here */
    setup_gpio();
    setup_dac();
    init_wavetable();
    setup_timer2();
    setup_dma();
    while(1){

    }

}
