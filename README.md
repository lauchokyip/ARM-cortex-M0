# ARM-cortex-M0

ARM cortex-M0 on STM32F0DISCOVERY board

List of things that I have worked on:<br>

- [Blink PC8 LED using SYSTICK](https://github.com/lauchokyip/ARM-cortex-M0/blob/master/Blink_LED_SYSTICK/src/main.s)
- [Blink PC8 LED using TIM6](https://github.com/lauchokyip/ARM-cortex-M0/blob/master/Blink_LED_TIM6/src/main.s)
- [Debouncing Keypad using software](https://github.com/lauchokyip/ARM-cortex-M0/blob/master/Debouncing_Keypad/src/main.c)
- [Simple Waveform Using TIM2 Interrupt](https://github.com/lauchokyip/ARM-cortex-M0/blob/master/TIM2_Interrupt_Wave/main.c)
- [Simple Waveform using DMA transfer](https://github.com/lauchokyip/ARM-cortex-M0/blob/master/TIM2_Invoke_DMA_DAC/main.c)
- [Final Project-Plotter](https://github.com/Andrew-Gan/frm-45)
